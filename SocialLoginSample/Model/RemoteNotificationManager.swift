//
//  RemoteNotificationManager.swift
//  SocialLoginSample
//
//  Created by Pooja Rana on 12/04/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

class RemoteNotificationManager: NSObject {

    static let root = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController!
    class func handleRemoteNotification(response: [AnyHashable : Any]) {
        print(response)
    }
}
