//
//  LoginViaFBModel.swift
//  SocialLoginSample
//
//  Created by Pooja Rana on 20/04/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class LoginViaFBModel: NSObject {
    
    static let shared = LoginViaFBModel()
    private override init() { }
    
    var success: (([String: Any]) -> Void)?
    var failure: ((String) -> Void)?
    
    func addFBLoginButton(sender: UIView) {
        let loginFbButton = FBSDKLoginButton()
        loginFbButton.frame = CGRect(x:sender.frame.width/2-loginFbButton.frame.width/2, y: 260, width: loginFbButton.frame.width, height: loginFbButton.frame.height)
        sender.addSubview(loginFbButton)
    }
    
    func viaGraphAPI(sender: UIViewController, success: (([String: Any]) -> Void)?, failure: ((String) -> Void)?) {
        
        self.success = success
        self.failure = failure
        
        if (FBSDKAccessToken.current() != nil) {
            // User is already logged in, do work such as go to next view controller.
            print("Already logged in")
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                error != nil ? self.failure?(error?.localizedDescription ?? "") : self.success?((result as? [String: Any])!)
            })
        } else {
            //Perform Login
            FBSDKLoginManager().logIn(withReadPermissions: ["public_profile", "email"], from: sender) { (result, error) in
                if error != nil {
                    self.failure?(error?.localizedDescription ?? "")
                    
                } else if (result?.isCancelled)! {
                    self.failure?("Cancelled")
                    
                } else {
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                        error != nil ? self.failure?(error?.localizedDescription ?? "") : self.success?((result as? [String: Any])!)
                    })
                }
            }
        }
    }
}
