//
//  LoginViaGoogleModel.swift
//  SocialLoginSample
//
//  Created by Pooja Rana on 24/04/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit
import GoogleSignIn

class LoginViaGoogleModel: NSObject, GIDSignInDelegate, GIDSignInUIDelegate {

    static let shared = LoginViaGoogleModel()
    private override init() { sender = nil }

    var sender: UIViewController?
    var success: (([String: Any]) -> Void)?
    var failure: ((String) -> Void)?

    func googleLoginSetUp(sender: UIViewController) {
        // Initialize sign-in
        self.sender = sender
        GIDSignIn.sharedInstance().clientID = "31692136630-67vliq0ig7rmn33ph8tb68q2rjak74tn.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }

    func initiateGoogleSignIN(success: (([String: Any]) -> Void)?, failure: ((String) -> Void)?) {
        self.success = success
        self.failure = failure
        GIDSignIn.sharedInstance().signIn()
    }

    // MARK: -  GoogleSignIn Delegate 
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        sender?.present(viewController, animated: true, completion: nil)
    }

    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        sender?.dismiss(animated: true, completion: nil)
    }

    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        guard error == nil else {
            self.failure?("Error while trying to redirect")
            return
        }
        print("Successful Redirection")
    }

    //MARK: GIDSignIn Delegate
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
    {
        if (error == nil) {
            // Perform any operations on signed in user here.
            self.success?(["userID": user.userID, "idToken": user.authentication.idToken, "name": user.profile.name, "givenName": user.profile.givenName, "familyName": user.profile.familyName, "email": user.profile.email])

        } else {
            self.failure?(error.localizedDescription)
        }
    }

    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }
}

