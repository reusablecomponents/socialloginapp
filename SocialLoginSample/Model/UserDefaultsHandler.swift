//
//  UserDefaultsHandler.swift
//  SocialLoginSample
//
//  Created by Pooja Rana on 12/04/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

class UserDefaultsHandler: NSObject {

    static let memory = UserDefaults.standard
    
    static var deviceToken: String? {
        get {
            return memory.value(forKey: "device_token") as? String ?? "Simulator1234567890"
        }
        set (token) {
            memory.setValue(token, forKey: "device_token")
            memory.synchronize()
        }
    }    
}
