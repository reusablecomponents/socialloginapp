//
//  SocialLoginController.swift
//  SocialLoginSample
//
//  Created by Pooja Rana on 16/04/18.
//  Copyright © 2018 Pooja Rana. All rights reserved.
//

import UIKit

class SocialLoginController: UIViewController {
    
    // MARK: -  ViewLifeCycle 
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        LoginViaFBModel.shared.addFBLoginButton(sender: self.view)
        LoginViaGoogleModel.shared.googleLoginSetUp(sender: self)
    }
    
    // MARK: -  IBOutlet Action 
    @IBAction func loginViaGoogle(_ sender: Any) {
        LoginViaGoogleModel.shared.initiateGoogleSignIN(success: { userInfo in
            print(userInfo["userID"] as? String ?? "") // For client-side use only!
            print(userInfo["idToken"] as? String ?? "") // Safe to send to the server
            
            print(userInfo["name"] as? String ?? "")
            print(userInfo["givenName"] as? String ?? "")
            print(userInfo["familyName"] as? String ?? "")
            print(userInfo["email"] as? String ?? "")
            
        }, failure: { error in
            print(error)
        })
    }
    
    @IBAction func loginViaGraphAPI(_ sender: Any) {
        LoginViaFBModel.shared.viaGraphAPI(sender: self, success: { response in
            let userID = response["id"] as! String
            let facebookProfileUrl = "http://graph.facebook.com/\(userID)/picture?type=large"
            print(facebookProfileUrl)
            
        }) { error in
            print(error)
        }
    }    
}
