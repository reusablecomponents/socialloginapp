SocialLoginSample works on Swift4.0 and requires ARC to build. It depends on the following Apple frameworks, which should already be included with most Xcode templates:

Foundation.framework
UIKit.framework

This sample will give you idea how to use FB and google frameworks to login.
This sample also provide firebase notificatin setup.

//*****************Firebase********************
The main guideline you need to follow when dealing with firebase notification is:
Add
pod 'Firebase/Core'
pod 'Firebase/Messaging'

Use these lines in your code to Register APNS and firebase setup.
1. self.registerPush(application: application)
2. Add Extension of Appdelegate
3. RemoteNotificationManager file

//*****************FB********************
You can directly add the LoginViaFBModel.swift source file to your project
The main guideline you need to follow when dealing with FB is:
Add
pod 'FBSDKLoginKit'

Use these lines in your code to get login setup for FB
LoginViaFBModel.shared.addFBLoginButton(sender: self.view)

Use this callbacks to access back the information fetched via Facebook:
LoginViaFBModel.shared.viaGraphAPI(sender: self, success: { response in

}) { error in
print(error)
}


//*****************Google********************
You can directly add the LoginViaGoogleModel.swift source file to your project
Use these lines in your code to get login setup for Google
Add
pod 'GoogleSignIn'

Use these lines in your code to get login setup for Google
LoginViaGoogleModel.shared.googleLoginSetUp(sender: self)

Use this callbacks to access back the information fetched via Google Sign In:
LoginViaGoogleModel.shared.initiateGoogleSignIN(success: { userInfo in

}, failure: { error in
print(error)
})
